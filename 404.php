<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Smile_English
 */

get_header();
?>
<!--
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'smile-english' ); ?></h1>
				</header>

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'smile-english' ); ?></p>

					<?php
					get_search_form();

					the_widget( 'WP_Widget_Recent_Posts' );
					?>

					<div class="widget widget_categories">
						<h2 class="widget-title"><?php esc_html_e( 'Most Used Categories', 'smile-english' ); ?></h2>
						<ul>
							<?php
							wp_list_categories( array(
								'orderby'    => 'count',
								'order'      => 'DESC',
								'show_count' => 1,
								'title_li'   => '',
								'number'     => 10,
							) );
							?>
						</ul>
					</div>
					<?php
					/* translators: %1$s: smiley */
					$smile_english_archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'smile-english' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$smile_english_archive_content" );

					the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div>
			</section>

		</main>
	</div>
-->

<main class="content">
	<!-- <aside class="aside__news"> -->
		<!-- <div>
			<h4 class="aside__news--header">новости</h4>
			<div class="aside__news-con">
				<div class="aside__news--arrow">
					<button id="prev" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
				</div>
				<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 300px;">
				<?php
					$i = 1;
					function position($a){
						if ($a==1){
							echo "left: 0;";
						} else {
							echo "left: 300px";
						}
					}

					$myposts = get_posts( array(
						'category' => 4
					) );

					foreach( $myposts as $post ){
					setup_postdata( $post );
					$a = True;
						if ($i == 5) {
							$a = False;
						}
					?>	
					<div class="aside__news__n-one" id="block<?php echo($i);?>" style="<?php position($i);?>">
						<a href="<?php the_permalink() ?>" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
						<h5 class="n-one--h5"><?php the_title(); ?></h5>
						<p class="n-one--p">
							<?php
								the_excerpt();
							?>
						</p>
					</div>
					<?php
						$i++;
					}
						wp_reset_postdata();
					?>
				</div>

				<div class="aside__news--arrow">
					<button class="aside__news--btn" id="next" href="#"><i class="fas fa-angle-right"></i></button>
				</div>
			</div>
		</div>
		<div style="margin-top: 20px;">
			<h4 class="aside__news--header">акции</h4>
			<div class="aside__news-con">
				<div class="aside__news--arrow">
					<button id="prev-a" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
				</div>
				<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 300px;">
				<?php
					$i = 1;

					$myposts = get_posts( array(
						'category' => 3
					) );

					foreach( $myposts as $post ){
					setup_postdata( $post );
					$a = True;
						if ($i == 5) {
							$a = False;
						}
					?>	
					<div class="aside__news__n-one" id="blocka<?php echo($i);?>" style="<?php position($i);?>">
						<a href="<?php the_permalink() ?>" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
						<h5 class="n-one--h5"><?php the_title(); ?></h5>
						<p class="n-one--p">
							<?php
								the_excerpt();
							?>
						</p>
					</div>
					<?php
						$i++;
					}
						wp_reset_postdata();
					?>
				</div>

				<div class="aside__news--arrow">
					<button class="aside__news--btn" id="next-a" href="#"><i class="fas fa-angle-right"></i></button>
				</div>
			</div>
		</div>
		 -->
	<!-- </aside> -->

	<article class="one-article">
		<h1 class="one-article--h1-404" style="">
			ошибка 404
		</h1>

		<div class="one-article__content">
			<p>
				
			Данной страницы не существует
			</p>
		</div>
	</article>
</main>
<?php
get_footer();
