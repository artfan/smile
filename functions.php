<?php
/**
 * Smile English functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Smile_English
 */

if ( ! function_exists( 'smile_english_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function smile_english_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Smile English, use a find and replace
		 * to change 'smile-english' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'smile-english', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Главное', 'smile-english' ),
			'menu-footer-1' => esc_html__( 'Подвал  взрослые', 'smile-english' ),
			'menu-footer-2' => esc_html__( 'Подвал  детское отделение', 'smile-english' ),
			'menu-footer-3' => esc_html__( 'Подвал  все ссылки', 'smile-english' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'smile_english_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		// add_theme_support( 'custom-logo', array(
		// 	'class'      => 'header__logo',

		// ) );
		add_theme_support( 'custom-logo', array(
		 	'class'      => 'header__logo',

		) );
	}
endif;
add_action( 'after_setup_theme', 'smile_english_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function smile_english_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'smile_english_content_width', 640 );
}
add_action( 'after_setup_theme', 'smile_english_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */


/**
 * Enqueue scripts and styles.
 */
function smile_english_scripts() {
	//wp_enqueue_style( 'smile-english-style', get_stylesheet_uri() );

// Подключаем свои стили
	wp_enqueue_style( 'normilize_styles', get_template_directory_uri() . '/css/normalize.css' );
	wp_enqueue_style( 'main_style', get_template_directory_uri() . '/css/style.css' );
	wp_enqueue_style( 'slick_style', get_template_directory_uri() . '/css/slick.css' );
	wp_enqueue_style( 'slick-theme_style', get_template_directory_uri() . '/css/slick-theme.css' );
	

	// wp_enqueue_style( 'icons', 'https://use.fontawesome.com/releases/v5.6.3/css/all.css');
// Подключаем jquery.min
	wp_enqueue_script( 'smile-english-jquery', get_template_directory_uri() . '/js/jquery.min.js');
	wp_enqueue_script( 'smile-english-slick', get_template_directory_uri() . '/js/slick.js');
	// wp_enqueue_script( 'smile-english-main', get_template_directory_uri() . '/js/main.js');

	wp_enqueue_script( 'smile-english-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'smile_english_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
