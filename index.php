<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>
<main>
	<?php echo do_shortcode('[metaslider id="222"]');?>

	<div class="main__text">
		<h1 class="main__text--h1">Языковые курсы в Абкане для всех уровней!</h1>
	</div>
	<div class="pluses">
		<div class="pluses__one">
			<h2 class="pluses__one--h2">ВЫСОКОЕ КАЧЕСТВО</h2>
			<p class="pluses__one--p">Полное погружение в английский язык с его носителями.</p>
		</div>
		<div class="pluses__one">
			<h2 class="pluses__one--h2">ДОСТУПНАЯ ЦЕНА</h2>
			<p class="pluses__one--p">Качественное образование по доступным ценам.</p>
		</div><div class="pluses__one">
			<h2 class="pluses__one--h2">ИНДИВИДУАЛЬНЫЙ ПОДХОД</h2>
			<p class="pluses__one--p">Оригинальный подход к каждому студенту.</p>
		</div><div class="pluses__one">
			<h2 class="pluses__one--h2">СПЕЦИАЛИСТЫ</h2>
			<p class="pluses__one--p">Высококвалифицированные специалисты с сертификатами международного образца.</p>
		</div>
	</div>
	<div class="nativs">
		<div class="nativs__block">
			<div class="block__one-person">
				<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/1.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">люси</span>
					<br>
					Франция
				</p>
			</div>
			<div class="block__one-person">
				<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/2.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Кип</span>
					<br>
					США
				</p>
			</div>
		</div>
		<div class="nativs__block">
			<div class="block__one-person">
				<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/6.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Софи</span>
					<br>
					Германия
				</p>
			</div>

			<div class="block__one-person">
				<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/7.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Джорджио</span>
					<br>
					Италия
				</p>
			</div>

		</div>
		<div class="nativs__block">
			<div class="block__text">
				<h2 class="block__text--h2">
					Изучайте языки с носителями!
				</h2>
				<p class="block__text--p">
					К нам часто приезжают друзья из разных стран и они все готовы пообщаться с вами!
				</p>
			</div>
			<div class="nativ__block--imgs">
				<div class="block__one-person">
					<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/3.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Рудольф</span>
					<br>
					Швейцария
				</p>
			</div>
				<div class="block__one-person">
					<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/4.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Ропел</span>
					<br>
					ЮАР
				</p>
			</div>
				<div class="block__one-person">
					<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/5.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Сандер</span>
					<br>
					Бельгия
				</p>
			</div>
			</div>
		</div>
		<div class="nativs__block">
			<div class="block__one-person">
				<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/8.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Конни</span>
					<br>
					США
				</p>
			</div>
			<div class="block__one-person">
				<img class="one-person--img" src="<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/peoples/9.jpg" alt="">
				<p class="one-person--info">
					<span class="one-person--info-name">Грегор</span>
					<br>
					Германия
				</p>
			</div>
		</div>
		
	</div>
	<div class="how">
		<header>
			<h2 class="how--h2">
				Наши программы обучения:
			</h2>
		</header>
		<div class="how-cont">
			<div class="how-one">
				<h3>Групповые занятия</h3>
				<p>Занятия проходят в комфортных и просторных классах. Максимальное количество студентов в одной группе 6-7 человек. Занятия проходят 2 раза в неделю по программам Cambridge, Oxford и Longman.  И самое главное, наши учителя не читают “лекции” за ограниченное время, а обучают вас иностранному языку, подбирая к каждому индивидуальный подход!</p>
			</div>
			<div class="how-one">
				<h3>Индивидуальные занятия</h3>
				<p>Занятия проходят 2 раза в неделю или по договоренности с преподавателем. Педагог подберет программу обучения по вашим критериям (для школы, университета, туризма и т.д.). Мы поможем понять язык, заполнить пробелы, вспомнить или начать с ноля.</p>
			</div>
			<div class="how-one">
				<h3>Детское обучение</h3>
				<p>Наша школа рада всем и даже самым маленьким! Уникальные и познавательные уроки в мини-группах (3-7 чел), созданные по зарубежной методике помогут вашему малышу приобщиться и полюбить английский язык. Кроме того это отличная подготовка к школе!</p>
			</div>
			<div class="how-one">
				<h3>Подготовка к экзаменам</h3>
				<p>Программа обучения всегда актуальна и соответствует новым требованиям к экзаменам.</p>
			</div>
			
		</div>
	</div>
	<div class="pluses-more">
		<a href="/programms/" class="pluses-more--a">
			Подробнее
		</a>
	</div>
<style>
	.slick-slide div{
		display: flex;
		align-items: center;
		justify-content: center;
	}
</style>
	<div class="prepods">
		<header>
			<h2 class="prepods--h2">Наша команда</h2>
		</header>
		<div class="prepods-con">
		<!-- <div class="" style="position: relative;"> -->
			<button id="prev-prepod" class="prepods-con__btn">
				<!-- <i class="fas fa-chevron-left"></i> -->
				<
			</button>
			<button id="next-prepod" class="prepods-con__btn">
				<!-- <i class="fas fa-chevron-right"></i> -->
				>
			</button>
		<div class="slider-con" style="">
			
			<?php 
				$i=1;
				$myposts = get_posts( array(
							'numberposts' => 100,
							// 'category' => 6
							'category' => 17
						) );
						$pos = 0;

						foreach( $myposts as $post ){
							setup_postdata( $post );
							$id = $post->ID;
						?>
						<div class="prepods__pre-one">
							<a href="/teachers#prepod<?php echo($i);?>" style="z-index: 5; width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
						<!-- <div class="" style="border: 1px solid red; box-sizing: border-box; border-right-color: blue; border-right-width: 5px;"> -->
							<div class="pre-one__desc">
								<h4 class="desc__name--h4"><?php the_title(); ?></h4>
								<?php
									$value = get_field( "квалификация" ); 
									if ($value): ?>
									<p class="desc__name--p">
										<?php the_field('квалификация');?>
									</p>

								<?php endif ?>
							</div>
							<?php 
								$image1 = get_field('фото1');
								$image2 = get_field('фото2');
							?>

									<img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" class="pre-one__img pre-one__img--first"/>
									<img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" class="pre-one__img pre-one__img--second"/>
									
									
						</div>
						<?php $i++; } ?>

			
		</div>
			
		</div>
		<a class="prepods--a" href="http://g90391sl.bget.ru/teachers/">Все сотрудники</a>
	</div>
<script>

</script>

	<div class="news">
		<header>
			<h2 class="news--h2">Наши новости</h2>
		</header>
		<div class="news-con">
			<?php
			$i = 1;
			query_posts('cat=4');
			if(have_posts()){ 
				$a = True;
				while(have_posts() && $a){
					if ($i == 5) {
						$a = False;
					}
					the_post();

			?>


			<article class="news-con__n-one">
				<a href="<?php the_permalink(); ?>">
				<?php
					$default_attr = array('class' => "n-one--img");
					the_post_thumbnail(array(200,200),$default_attr);
				?>
				</a>
				
				
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
				
				<?php
					the_excerpt();
				?>
				<a class="n-one--more" href="<?php the_permalink(); ?>">Подробнее</a>

			</article>
			<?php
				$i++;
			}

				wp_reset_postdata();
			}
			?>
			
		</div>
	</div>
	<!-- <div class="price">
		<header class="price-header">
			<h2 class="price--h2">Наши цены</h2>
		</header>
		<div class="price-con">
			<h3 class="price--h3">Английский язык для взрослых</h3>
			<table class="price__table">
				<thead class="table__head">
					<tr class="table__head--tr">
						<td class="table__head--td">Направление</td>
						<td class="table__head--td">Режим</td>
						<td class="table__head--td">Длительность занятия</td>
						<td class="table__head--td">Стоимость абоненмента <br> (8 занятий)</td>
					</tr>
					<tr class="table__head--color"></tr>
				</thead>
				<tbody>
					<?php
					$key_mode ="режим";
					$key_time ="длительность";
					$key_price ="цена";
					$myposts = get_posts( array(
						'category' => 19
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

					?>
					<tr class="table--tr">
						<td class="table--td table--td-m"><?php the_title();?></td>
						<td class="table__head--td-m">Режим</td>

						<td class="table--td">
							<?php the_field($key_mode);?>
						</td>
						<td class="table__head--td-m">Длительность занятия</td>

						<td class="table--td">
							<?php the_field($key_time);?>
						</td>
						<td class="table__head--td-m">Стоимость абоненмента <br> (8 занятий)</td>

						<td class="table--td">
							<?php the_field($key_price); ?> ₽
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table> -->
			
<!-- <div class="mobile-price">
				<h2 class="mobile-price--h3">
					<?php the_title(); ?>
				</h2>
				<p class="price-m--head">
					Режим
				</p>
				<p class="mobile-price--p">
					<?php the_field($key_mode);?>
				</p>
				<p class="price-m--head">
					Длительность занятия
				</p>
				<p class="mobile-price--p">
					<?php the_field($key_time);?>
				</p>
				<p class="price-m--head">
					Стоимость абоненмента <br> (8 занятий)
				</p>
				<p class="mobile-price--p">
					<?php the_field($key_price);?>
				</p>
</div> -->
			<!-- <h3 class="price--h3">Английский язык для детей</h3>
			<table class="price__table">
				<thead class="table__head">
					<tr class="table__head--tr">
						<td class="table__head--td">Направление</td>
						<td class="table__head--td">Режим</td>
						<td class="table__head--td">Длительность занятия</td>
						<td class="table__head--td">Стоимость абоненмента <br> (8 занятий)</td>
					</tr>
				</thead>
				<tbody>
					<?php
					$myposts = get_posts( array(
						'category' => 20
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

					?>
					<tr class="table--tr">
						<td class="table--td"><?php the_title();?></td>
						<td class="table--td">
							<?php the_field($key_mode);?>
						</td>
						<td class="table--td">
							<?php the_field($key_time);?>
						</td>
						<td class="table--td">
							<?php the_field($key_price);?> ₽
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table> -->

<!-- 		 	<h3 class="price--h3">Индивидуальные занятия</h3>
			<table class="price__table">
				<thead class="table__head">
					<tr class="table__head--tr">
						<td class="table__head--td">Направление</td>
						<td class="table__head--td">Режим</td>
						<td class="table__head--td">Длительность занятия</td>
						<td class="table__head--td">Стоимость абоненмента <br> (8 занятий)</td>
					</tr>
				</thead>
				<tbody>
					<?php
					$myposts = get_posts( array(
						'category' => 20
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

					?>
					<tr class="table--tr">
						<td class="table--td"><?php the_title();?></td>
						<td class="table--td">
							<?php the_field($key_mode);?>
						</td>
						<td class="table--td">
							<?php the_field($key_time);?>
						</td>
						<td class="table--td">
							<?php the_field($key_price);?> ₽
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>   -->
	<!-- 	</div>
 
	</div> -->
 
	<section class="callback">
		<div class="callback-con">
			<header class="callback__header">
				<h2 class="callback__header--h2">
					Записаться на бесплатный урок
				</h2>
			</header>
			
			<!-- 	<form class="form" action="test.php">
					<div class="form__name">
						<label class="form__name--label" for="name" >Ваше имя</label>
						<input name="name" type="text" required placeholder="Иванов Иван">
					</div>
					<div class="form__phone">
						<label class="form__phone--label" for="telephon" >Ваш телефон</label>
						<input name="telephon" type="tel" required placeholder="8 900 876 54 32">
					</div>
					<input type="submit">
				</form> 
			-->

			<div class="form">
					
			<?php
				echo do_shortcode('[contact-form-7 id="52" title="ФОС на главной"]');
			?>
			</div>
	</section>


<!--     <div id="map" class="map"></div> 

<script src="https://api-maps.yandex.ru/2.1/?apikey=<ваш API-ключ>&lang=ru_RU" type="text/javascript">
</script>
<script type="text/javascript"> 
        ymaps.ready(init);
		function init() {
		    var myMap = new ymaps.Map("map", {	    	
		            // center: [53.737858, 91.422821],
		            center: [53.737414, 91.423565],
		            controls: [],

		            zoom: 17 
		        }, { 
		            searchControlProvider: 'yandex#search'
		        }),

		    Torosova = new ymaps.Placemark([53.738102, 91.419391], {
			            hintContent: 'Вход со двора',
			            balloonContent: 'Торосова 9, вход со двора',
			            iconContent: ''
			}, {
		            draggable: false,
			        iconLayout: 'default#imageWithContent',
			        iconImageHref: '<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/logo.png',
					iconImageSize: [77, 70],
			        iconImageOffset: [-35, -35],
			});

				KatPer = new ymaps.Placemark([53.737119, 91.428072], {
			        hintContent: 'Кати Перекрещенко 7',
			        balloonContent: 'Кати Перекрещенко 7',
			        iconContent: ''
			    }, {
		            draggable: false,
			        iconLayout: 'default#imageWithContent',
			        iconImageHref: '<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/logo.png',
					iconImageSize: [77, 70],
			        iconImageOffset: [-35, -35],
			    });
		    myMap.geoObjects
		    .add(KatPer)
		    .add(Torosova);
		}     
</script>  -->
</main>

<?php

get_footer();
