<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Smile_English
 */
if (get_the_ID() <> 15) { ?>
	<div id="map" class="map"></div>
	<script src="https://api-maps.yandex.ru/2.1/?apikey=<ваш API-ключ>&lang=ru_RU" type="text/javascript">
	</script>
	<script type="text/javascript"> 
        ymaps.ready(init);
		function init() {
		    var myMap = new ymaps.Map("map", {	    	
		            // center: [53.737858, 91.422821],
		            center: [53.737414, 91.423565],
		            controls: [],

		            zoom: 17 
		        }, { 
		            searchControlProvider: 'yandex#search'
		        }),

		    Torosova = new ymaps.Placemark([53.738102, 91.419391], {
			            hintContent: 'Вход со двора',
			            balloonContent: 'Торосова 9, вход со двора',
			            iconContent: ''
			}, {
		            draggable: false,
			        iconLayout: 'default#imageWithContent',
			        iconImageHref: '<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/logo.png',
					iconImageSize: [77, 70],
			        iconImageOffset: [-35, -35],
			});

				KatPer = new ymaps.Placemark([53.737119, 91.428072], {
			        hintContent: 'Кати Перекрещенко 7',
			        balloonContent: 'Кати Перекрещенко 7',
			        iconContent: ''
			    }, {
		            draggable: false,
			        iconLayout: 'default#imageWithContent',
			        iconImageHref: '<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/logo.png',
					iconImageSize: [77, 70],
			        iconImageOffset: [-35, -35],
			    });
		    myMap.geoObjects
		    .add(KatPer)
		    .add(Torosova);
		}     
	</script> 
<?php
	}
 ?>

<footer class="footer">
	<div class="footer-con">
		<div class="footer__adult">
			<h5 class="footer__adult--h5">Взрослое отделение</h5>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-footer-1',
					'container_class' => 'footer__adult',
					'menu_class' => 'footer__adult--ul',
				) );
			?>
			
		</div>
		<div class="footer__kids">
			<h5 class="footer__kids--h5">Детское отделение</h5>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-footer-2',
					'container_class' => 'footer__kids',
					'menu_class' => 'footer__kids--ul',
				) );
			?>
			
		</div>
		<div class="footer__info">
			<h5 class="footer__info--h5">Меню</h5>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-footer-3',
					'container_class' => 'footer__info',
					'menu_class' => 'footer__info--ul',
				) );
			?>
			
		</div>
		<div class="footer__copy">
			<div class="copy__soc">
				<a href="https://vk.com/smileeng" class="copy__soc--a " target="_blank"><i class="fab1 fab fa-vk"></i></a>
				<a href="https://www.facebook.com/groups/1744453485796586" class="copy__soc--a " target="_blank"><i class="fab1 fab fa-facebook-f"></i></a>
				<a href="https://www.instagram.com/smileenglishabakan/" class="copy__soc--a " target="_blank"><i class="fab1 fab fa-instagram"></i></a>
			</div>
			<div class="copy__contacts">
				<a href="tel:89024670055" class="copy__contacts--a">8 (902) 467-00-55</a>
				<a href="tel:83902260055" class="copy__contacts--a"> 8 (3902) 26-00-55</a>
				<a href="#" class="copy__contacts--callback">Заказать звонок</a>
			</div>
			<div class="copy__copyright">
				<p class="copy__copyright--p copy__copyright--pc">&copy; 2010-2019 SmileEnglish</p>
				<p class="copy__copyright--p">Продолжая использовать наш сайт, вы даёте согласие на обработку <a href="#" class="copy__copyright--a">файлов Cookies</a> и других пользовательских данных, в соответствии с <a href="#" class="copy__copyright--a">Политикой конфиденциальности.</a></p>
			</div>
			
		</div>
	</div>
</footer>


<?php 
	wp_enqueue_script( 'smile-english-main', get_template_directory_uri() . '/js/main.js');
	wp_footer(); ?>

</body>
</html>
