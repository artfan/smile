<?php
/*
Template Name: Шаблон программы
Template Post Type: post, page, product

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>
<!-- <script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
 -->
<?php
	the_post();
?>
<header class="program__header">
	<img class="program__header--img" src="<?php echo (get_the_post_thumbnail_url());?>" alt="">
	<h1 class="program__header--h1">
		<?php 
			the_title();
		?>
	</h1>
		<?php 
			the_excerpt();

	switch (get_the_ID()) {
		case '194': #тренинговые занятия
			echo do_shortcode('[contact-form-7 id="46" title="Запись на тренинговое занятие"]');
			break;
		case '196': #разговорные клубы взрослые
			echo do_shortcode('[contact-form-7 id="45" title="Запись на разговорный клуб взрослые"]');
			break;
		case '198': #euroexam
			echo do_shortcode('[contact-form-7 id="423" title="Запись на урок подготовка euroexam"]');
			break;
		case '190': #для путешествий
			echo do_shortcode('[contact-form-7 id="51" title="Запись на урок для путешествий"]');
			break;
		case '200': #общий курс для взрослых
			echo do_shortcode('[contact-form-7 id="55" title="Запись на урок общий курс"]');
			break;
		case '171': #разговорные клубы дети
			echo do_shortcode('[contact-form-7 id="54" title="Запись на урок разговорный клуб детский"]');
			break;
		case '173':#14-17 лет
			echo do_shortcode('[contact-form-7 id="48" title="Запись на урок 14-17 лет"]');
			break;
		case '185': #11-13
			echo do_shortcode('[contact-form-7 id="47" title="Запись на урок 11-13"]');
			break;
		case '185': #7-10лет
			echo do_shortcode('[contact-form-7 id="50" title="Запись на урок 7-10"]');
			break;
		case '188':#3-6лет
			echo do_shortcode('[contact-form-7 id="49" title="Запись на урок 3-6"]');
			break;
		
		default:
			# code...
			break;
	}
		
	?>

</header>
<main class="content">
	<aside class="aside__news">
		<div>
				
			<h4 class="aside__news--header">новости</h4>
			<div class="aside__news-con">
				<div class="aside__news--arrow">
					<button id="prev" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
				</div>
				<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 300px;">
				<?php
					$i = 1;
					function position($a){
						if ($a==1){
							echo "left: 0;";
						} else {
							echo "left: 300px";
						}
					}

					$myposts = get_posts( array(
						'category' => 4
					) );

					foreach( $myposts as $post ){
					setup_postdata( $post );
					$a = True;
						if ($i == 5) {
							$a = False;
						}
					?>	
					<div class="aside__news__n-one" id="block<?php echo($i);?>" style="<?php position($i);?>">
						<a href="<?php the_permalink() ?>" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
						<h5 class="n-one--h5"><?php the_title(); ?></h5>
						<p class="n-one--p">
							<?php
								the_excerpt();
							?>
						</p>
					</div>
					<?php
						$i++;
					}
						wp_reset_postdata();
					?>
				</div>

				<div class="aside__news--arrow">
					<button class="aside__news--btn" id="next" href="#"><i class="fas fa-angle-right"></i></button>
				</div>
			</div>
		</div>

		<div style="margin-top: 20px;">
			<h4 class="aside__news--header">акции</h4>
			<div class="aside__news-con">
				<div class="aside__news--arrow">
					<button id="prev-a" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
				</div>
				<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 300px;">
				<?php
					$i = 1;

					$myposts = get_posts( array(
						'category' => 3
					) );

					foreach( $myposts as $post ){
					setup_postdata( $post );
					$a = True;
						if ($i == 5) {
							$a = False;
						}
					?>	
					<div class="aside__news__n-one" id="blocka<?php echo($i);?>" style="<?php position($i);?>">
						<a href="<?php the_permalink() ?>" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
						<h5 class="n-one--h5"><?php the_title(); ?></h5>
						<p class="n-one--p">
							<?php
								the_excerpt();
							?>
						</p>
					</div>
					<?php
						$i++;
					}
						wp_reset_postdata();
					?>
				</div>

				<div class="aside__news--arrow">
					<button class="aside__news--btn" id="next-a" href="#"><i class="fas fa-angle-right"></i></button>
				</div>
			</div>
		</div>
		
	</aside>
	<div class="program__content">
		<?php
			the_content();
		?>	
	</div>
</main>


		<!-- </main>#main -->
	<!-- </div>#primary -->
<script>
	var x=0;
	$('#next').click(function (){
		x++;
		switch(x) {
		  case 1:
		    $('#block1').css('left','-300px');
			$('#block2').css('left','0px');
		    break;

		  case 2:
			$('#block2').css('left','-300px');
			$('#block3').css('left','0px');
		    break;

		  case 3:
			$('#block3').css('left','-300px');
			$('#block4').css('left','0px');
		    break;
		   default:
		   	x--;
		   break;
		}
	});
	$('#prev').click(function(){
		switch(x) {
		  case 1:   
		    $('#block1').css('left','0px');
			$('#block2').css('left','300px');
		    break;

		  case 2:  
			$('#block2').css('left','0px');
			$('#block3').css('left','300px');
		    break;

		  case 3:
			$('#block3').css('left','0px');
			$('#block4').css('left','300px');
		    break;

		  default:
		   	x++;
		   break;
		}
		x--;
	});
	var y=0;
	$('#next-a').click(function (){
		y++;
		switch(y) {
		  case 1:   
		    $('#blocka1').css('left','-300px');
			$('#blocka2').css('left','0px');
		    break;

		 //  case 2:  
			// $('#blocka2').css('left','-300px');
			// $('#blocka3').css('left','0px');
		 //    break;

		 //  case 3:
			// $('#blocka3').css('left','-300px');
			// $('#blocka4').css('left','0px');
		 //    break;

		   default:
		   	y--;
		   break;
		}
	});
	$('#prev-a').click(function(){
		switch(y) {
		  case 1:   
		    $('#blocka1').css('left','0px');
			$('#blocka2').css('left','300px');
		    break;

		 //  case 2:  
			// $('#blocka2').css('left','0px');
			// $('#blocka3').css('left','300px');
		 //    break;

		 //  case 3:
			// $('#blocka3').css('left','0px');
			// $('#blocka4').css('left','300px');
		 //    break;

		  default:
		   	y++;
		   break;
		}
		y--;
	});
</script>	

<span>
	



<?php
// get_sidebar();
get_footer();
