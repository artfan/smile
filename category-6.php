<?php
/*

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>
<section class="feedbacks">
	<div class="feedbacks__con">
		<header class="feedbacks__header">
			<h1 class="feedbacks__header--h1">Новости<?php //the_title();?></h1>
		</header>
	</div>

</section>
<main class="content">
	<?php
		get_sidebar();
	?>
	<aside class="aside__news">
		<div class="aside__news-con">
			<div class="aside__news--arrow">
				<button id="prev" class="aside__news--btn"><i class="fas fa-angle-left"></i></button>
			</div>
			<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 350px;">
			<?php
				$i = 1;
				function position($a){
					if ($a==1){
						echo "left: 0;";
					} else {
						echo "left: 300px";
					}
				}

				$myposts = get_posts( array(
					'category' => 6
				) );

				foreach( $myposts as $post ){
				setup_postdata( $post );
				$a = True;
					if ($i == 5) {
						$a = False;
					}
				?>	
				<div class="aside__news__n-one" id="block<?php echo($i);?>" style="<?php position($i);?>">
					<a href="<?php the_permalink(); ?>">
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
					</a>
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
					
						<?php
							the_excerpt();
						?>
					
					<a href="<?php the_permalink(); ?>" class="n-one--more">Читать далее</a>
				</div>
				<?php
					$i++;
				}

					wp_reset_postdata();
				// }
				?>
			</div>
			<div class="aside__news--arrow">
				<button class="aside__news--btn" id="next"><i class="fas fa-angle-right"></i></button>
			</div>
		</div>

		<div class="aside__news-con">
			<div class="aside__news--arrow">
				<button id="prev-a" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
			</div>
			<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 350px;">
			<?php
				$i = 1;
				$a = true;
				
				$myposts = get_posts( array(
					'category' => 8
				) );

				foreach( $myposts as $post ){
				setup_postdata( $post );
				$a = True;
					if ($i == 5) {
						$a = False;
					}
				?>	
				<div class="aside__news__n-one" id="blocka<?php echo($i);?>" style="<?php position($i);?>">
					<a href="<?php the_permalink(); ?>">
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
					</a>
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
					
						<?php
							the_excerpt();
						?>
					
					<a href="<?php the_permalink(); ?>" class="n-one--more">Читать далее</a>
				</div>
				<?php
					$i++;
				}

					wp_reset_postdata();
				// }
				?>
			</div>
			<div class="aside__news--arrow">
				<button class="aside__news--btn" id="next-a" href="#"><i class="fas fa-angle-right"></i></button>
			</div>
		</div>
		
	</aside>

	<article class="one-article" style="border: none; padding: 0;">
		<div class="feedbaks__feed-all">

			<?php if ( have_posts() ) : 
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
			?>
				<article class="feed-all__one-feed">
					<?php if(has_post_thumbnail()){
						?>
						<div class="one-feed__img">
							<a href="<?php the_permalink() ?>">
							<?php
								$default_attr = array('class' => "one-feed__img--img");
								the_post_thumbnail(array(420,280),$default_attr);
							?>
							</a>
						</div>
					<div class="one-feed__text">
						<a href="<?php the_permalink() ?>" style="text-decoration: none; color: #000;">
							<p class="one-feed__text--name-p"><?php the_title(); ?></p>
						</a>
						<!-- <p class="one-feed__text--text-p"> -->
							<?php 
								// the_content();
								the_excerpt();
							?>
							<div  style="display: flex; align-items: center; justify-content: center; margin: 20px 0;">
								<a class="n-one--more" href="<?php the_permalink(); ?>">Подробнее</a>
							</div>

						<!-- </p> -->
					</div>
					<?php } else{?>
						<div class="one-feed__text" style="width: 100%;">
						<a href="<?php the_permalink() ?>" style="text-decoration: none; color: #000;"><p class="one-feed__text--name-p"><?php the_title(); ?></p></a>
						<!-- <p class="one-feed__text--text-p"> -->
							<?php 
								// the_content();
								the_excerpt();
							?>
							<div style="display: flex; align-items: center; justify-content: center; margin: 20px 0;"> 
								<a class="n-one--more" href="<?php the_permalink(); ?>">Подробнее</a>
							</div>

						<!-- </p> -->
					</div>

					<?php } ?>
						
				</article>
	
			<?php
			endwhile;
			endif;
			?>
				<!-- <div class="one-feed__img">
					<img src="img/feedbacks/1.png" alt="" class="one-feed__img--img">
				</div> -->

		</div>
	</article>
</main>
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
	var x=0;
	$('#next').click(function (){
		x++;
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','-300px');
			$('#block2').css('left','0px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','-300px');
			$('#block3').css('left','0px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','-300px');
			$('#block4').css('left','0px');
			// alert(x);
		    break;
		   default:
		  	// alert(x);
		   	x--;
		   break;
		}
	});
	$('#prev').click(function moveClient(){
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','0px');
			$('#block2').css('left','300px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','0px');
			$('#block3').css('left','300px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','0px');
			$('#block4').css('left','300px');
			// alert(x);
		    break;
		  default:
		  	// alert(x);
		   	x++;
		   break;
		}
		x--;
	});

	var y=0;
	$('#next-a').click(function (){
		y++;
		switch(y) {
		  case 1:   // if (x === 'value1')
		    $('#blocka1').css('left','-300px');
			$('#blocka2').css('left','0px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#blocka2').css('left','-300px');
			$('#blocka3').css('left','0px');
			// alert(x);
		    break;

		  case 3:
			$('#blocka3').css('left','-300px');
			$('#blocka4').css('left','0px');
			// alert(x);
		    break;
		   default:
		  	// alert(x);
		   	y--;
		   break;
		}
	});
	$('#prev-a').click(function moveClient(){
		switch(y) {
		  case 1:   // if (x === 'value1')
		    $('#blocka1').css('left','0px');
			$('#blocka2').css('left','300px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#blocka2').css('left','0px');
			$('#blocka3').css('left','300px');
			// alert(x);
		    break;

		  case 3:
			$('#blocka3').css('left','0px');
			$('#blocka4').css('left','300px');
			// alert(x);
		    break;
		  default:
		  	// alert(x);
		   	y++;
		   break;
		}
		y--;
	});
	
</script>
<?php

get_footer();
