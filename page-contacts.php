<?php
/*
Шаблон подключен через slug

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>


<section id="primary" class="content-area contacts" >

	
	<?php
		the_post(); 
	?>

	<header class="contacts__header">
		<h1 class="contacts__header--h1">
			<?php the_title(); ?>
				
		</h1>
	</header>

	<?php
		the_content();
	?>

</section> <!-- primary -->

<script src="https://api-maps.yandex.ru/2.1/?apikey=<ваш API-ключ>&lang=ru_RU" type="text/javascript">
</script>

<script type="text/javascript"> 
        ymaps.ready(init);
		function init() {
		    var myMap = new ymaps.Map("map-contacts", {	    	
		            // center: [53.737858, 91.422821],
		            center: [53.737414, 91.423565],
		            controls: [],

		            zoom: 16
		        }, { 
		            searchControlProvider: 'yandex#search'
		        }),

		    Torosova = new ymaps.Placemark([53.738102, 91.419391], {
			            hintContent: 'Вход со двора',
			            balloonContent: 'Торосова 9, вход со двора',
			            iconContent: ''
			}, {
		            draggable: false,
			        iconLayout: 'default#imageWithContent',
			        iconImageHref: '<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/logo.png',
					iconImageSize: [77, 70],
			        iconImageOffset: [-35, -35],
			});

				KatPer = new ymaps.Placemark([53.737119, 91.428072], {
			        hintContent: 'Кати Перекрещенко 7',
			        balloonContent: 'Кати Перекрещенко 7',
			        iconContent: ''
			    }, {
		            draggable: false,
			        iconLayout: 'default#imageWithContent',
			        iconImageHref: '<?php echo (get_stylesheet_directory_uri()); ?>/inc/img/logo.png',
					iconImageSize: [77, 70],
			        iconImageOffset: [-35, -35],
			    });
		    myMap.geoObjects
		    .add(KatPer)
		    .add(Torosova);
		}     
</script> 



<?php
// get_sidebar();
get_footer();
