<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Smile_English
 */

if ( ! is_active_sidebar( 'sidebar-news' ) ) {
	return;
}
?>
<?php //dynamic_sidebar( 'sidebar-news' ); ?>
<div class="aside__news-con">
			<div class="aside__news--arrow">
				<button id="prev" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
			</div>
			<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 300px;">
			<?php
				$i = 1;
				function position($a){
					if ($a==1){
						echo "left: 0;";
					} else {
						echo "left: 300px";
					}
				}

				$myposts = get_posts( array(
					'category' => 6
				) );

				foreach( $myposts as $post ){
				setup_postdata( $post );
				$a = True;
					if ($i == 5) {
						$a = False;
					}
				?>	
				<div class="aside__news__n-one" id="block<?php echo($i);?>" style="<?php position($i);?>">
					<a href="<?php the_permalink() ?>" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
					<?php
						$default_attr = array('class' => "n-one--img");
						the_post_thumbnail(array(420,280),$default_attr);
					?>
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
					<p class="n-one--p">
						<?php
							the_excerpt();
						?>
					</p>
				</div>
				<?php
					$i++;
				}
					wp_reset_postdata();
				?>
			</div>

			<div class="aside__news--arrow">
				<button class="aside__news--btn" id="next" href="#"><i class="fas fa-angle-right"></i></button>
			</div>
		</div>

