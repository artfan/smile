<?php
/*


 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<main class="content">
	<aside class="aside__news">
		<div class="aside__news-con">
			<div class="aside__news--arrow">
				<button id="prev" class="aside__news--btn"><i class="fas fa-angle-left"></i></button>
			</div>
			<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 350px;">
			<?php
				$i = 1;
				function position($a){
					if ($a==1){
						echo "left: 0;";
					} else {
						echo "left: 300px";
					}
				}

				$myposts = get_posts( array(
					'category' => 6
				) );

				foreach( $myposts as $post ){
				setup_postdata( $post );
				$a = True;
					if ($i == 5) {
						$a = False;
					}
				?>	
				<div class="aside__news__n-one" id="block<?php echo($i);?>" style="<?php position($i);?>">
					<a href="<?php the_permalink(); ?>">
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
					</a>
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
					<p class="n-one--p">
						<?php
							the_excerpt();
						?>
					</p>
					<a href="<?php the_permalink(); ?>" class="n-one--more">Читать далее</a>
				</div>
				<?php
					$i++;
				}

					wp_reset_postdata();
				// }
				?>
			</div>

			<div class="aside__news--arrow">
				<button class="aside__news--btn" id="next"><i class="fas fa-angle-right"></i></button>
			</div>
		</div>
		<div class="aside__news-con">
			<div class="aside__news--arrow">
				<button id="prev-a" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
			</div>
			<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 350px;">
			<?php
				$i = 1;
				$a = true;
				
				$myposts = get_posts( array(
					'category' => 8
				) );

				foreach( $myposts as $post ){
				setup_postdata( $post );
				$a = True;
					if ($i == 5) {
						$a = False;
					}
				?>	
				<div class="aside__news__n-one" id="blocka<?php echo($i);?>" style="<?php position($i);?>">
					<a href="<?php the_permalink(); ?>">
						<?php
							$default_attr = array('class' => "n-one--img");
							the_post_thumbnail(array(420,280),$default_attr);
						?>
					</a>
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
					<p class="n-one--p">
						<?php
							the_excerpt();
						?>
					</p>
					<a href="<?php the_permalink(); ?>" class="n-one--more">Читать далее</a>
				</div>
				<?php
					$i++;
				}

					wp_reset_postdata();
				// }
				?>
			</div>
			<div class="aside__news--arrow">
				<button class="aside__news--btn" id="next-a" href="#"><i class="fas fa-angle-right"></i></button>
			</div>
		</div>
	</aside>

	<article class="one-article">
		<?php
		// wp_reset_postdata();

		// while ( have_posts() ) {
			the_post(); ?>

			<h1 class="one-article--h1"><?php the_title(); ?></h1>


			<div class="one-article__content">
				
			<?php 
				$default_attr = array('class' => "one-article--img");
				the_post_thumbnail(array(420,280),$default_attr);
			
				the_content();
		// }
			?>
			<div class="share-links">

				<a class="share-links--a" href="https://vk.com/share.php?url=http%3A%2F%2Fnew-smile%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d0%25b8%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d1%258c-5-2%2F&amp;title=%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D1%8C%205%20%E2%80%94%20Smile%20English&amp;utm_source=share2" rel="nofollow noopener" target="_blank" title="ВКонтакте"><svg xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 132 132"><title>ВКонтакте</title><g id="Слой_2" data-name="Слой 2"><g data-name="1"><path class="share-links--img share-links--vk" d="M9.6,0H122.4A9.6,9.6,0,0,1,132,9.6V122.4a9.6,9.6,0,0,1-9.6,9.6H9.6A9.6,9.6,0,0,1,0,122.4V9.6A9.6,9.6,0,0,1,9.6,0Zm91.3,83.3-.3-.5q-2-3.5-7.5-8.7h-.2l-3.2-3.2a3,3,0,0,1-.6-3.2,29.6,29.6,0,0,1,3.5-5.1l2.5-3.3q6.8-9,5.9-11.5l-.2-.4a2.3,2.3,0,0,0-1.2-.6,6.1,6.1,0,0,0-2.5-.1H84.3l-.4.4-.4.7a64.2,64.2,0,0,1-4.2,8.8L76.6,61a20.5,20.5,0,0,1-2,2.6L73.1,65c-.4.3-.8.5-1,.4l-.7-.2a2.6,2.6,0,0,1-.9-1,4.3,4.3,0,0,1-.5-1.5q-.1-.9-.1-1.6c0-.4,0-1.1,0-1.9s0-1.4,0-1.6,0-2.1.1-3.2V51.7q0-1,0-2.2a9.1,9.1,0,0,0-.1-1.8,6.1,6.1,0,0,0-.4-1.3,2.1,2.1,0,0,0-.8-.9L67.5,45a26.4,26.4,0,0,0-5.3-.5H60.7q-6,0-7.9.9a5.3,5.3,0,0,0-1.5,1.2q-.7.9-.2.9a4.8,4.8,0,0,1,3.3,1.7l.2.5a7.4,7.4,0,0,1,.5,1.8,18.1,18.1,0,0,1,.4,2.9,30.3,30.3,0,0,1,0,4.9q-.2,2-.4,3.2a6.4,6.4,0,0,1-.5,1.8l-.5.9-.2.2-1.1.2a2.5,2.5,0,0,1-1.3-.6A9.4,9.4,0,0,1,50,63.5a20.2,20.2,0,0,1-1.9-2.7q-1.1-1.7-2.2-4.1l-.6-1.1-1.6-3.2q-1-2.1-1.8-4.1a2.6,2.6,0,0,0-.9-1.3h-.2l-.6-.3-.9-.3H28.4a2.7,2.7,0,0,0-2.2.7l-.2.2a1.3,1.3,0,0,0-.1.6h0a3.1,3.1,0,0,0,.2,1q2.4,5.5,5.1,10.7t4.8,8.3q2,3.1,4.2,5.9L43,77.6l1.1,1.2,1,.9A23,23,0,0,0,47.9,82l4,2.6a22,22,0,0,0,5.2,2.1,17.8,17.8,0,0,0,5.7.6h4.5a3.1,3.1,0,0,0,2.1-.9l.2-.2a2.6,2.6,0,0,0,.3-.7,4,4,0,0,0,.1-1.1,13,13,0,0,1,.3-3,8,8,0,0,1,.7-2,4.9,4.9,0,0,1,.8-1.2l.7-.6h.3a2.5,2.5,0,0,1,2.2.6,13.1,13.1,0,0,1,2.4,2.3l2.7,3a18.7,18.7,0,0,0,2.7,2.5l.8.5,2,.9a4.4,4.4,0,0,0,2.2.2l10-.2a4.5,4.5,0,0,0,2.3-.5,1.9,1.9,0,0,0,1-1.1,3.1,3.1,0,0,0,0-1.3A5.1,5.1,0,0,0,100.9,83.3Z"/></g></g></svg></a>
					
				<a class="share-links--a" href="https://www.facebook.com/sharer.php?src=sp&amp;u=http%3A%2F%2Fnew-smile%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d0%25b8%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d1%258c-5-2%2F&amp;title=%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D1%8C%205%20%E2%80%94%20Smile%20English&amp;utm_source=share2" rel="nofollow noopener" target="_blank" title="Facebook"><svg xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 132 132">
						<title>Facebook</title>
						<g class="" data-name="Слой 2">
							<g id="_1" data-name="1">
								<path class="share-links--img" d="M9.6,0H122.4A9.6,9.6,0,0,1,132,9.6V122.4a9.6,9.6,0,0,1-9.6,9.6H9.6A9.6,9.6,0,0,1,0,122.4V9.6A9.6,9.6,0,0,1,9.6,0ZM83.7,30.6H74.5c-10.3,0-17,6.8-17,17.4v8H48.3a1.4,1.4,0,0,0-1.4,1.4V69.1a1.4,1.4,0,0,0,1.4,1.4h9.2V100a1.4,1.4,0,0,0,1.4,1.4H71a1.4,1.4,0,0,0,1.4-1.4V70.6H83.3a1.4,1.4,0,0,0,1.4-1.4V57.5a1.5,1.5,0,0,0-.4-1,1.4,1.4,0,0,0-1-.4H72.5V49.2c0-3.3.8-4.9,5-4.9h6.2a1.4,1.4,0,0,0,1.4-1.4V32A1.4,1.4,0,0,0,83.7,30.6Z"/>
							</g>
						</g>
					</svg></a>
					
				<a class="share-links--a" href="https://connect.ok.ru/offer?url=http%3A%2F%2Fnew-smile%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d0%25b8%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d1%258c-5-2%2F&amp;title=%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D1%8C%205%20%E2%80%94%20Smile%20English&amp;utm_source=share2" rel="nofollow noopener" target="_blank" title="Одноклассники"><svg xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 132 132"><title>Одноклассники</title><g id="Слой_2" data-name="Слой 2"><g id="_1" data-name="1"><path class="share-links--img" d="M9.6,0H122.4A9.6,9.6,0,0,1,132,9.6V122.4a9.6,9.6,0,0,1-9.6,9.6H9.6A9.6,9.6,0,0,1,0,122.4V9.6A9.6,9.6,0,0,1,9.6,0ZM62.3,81.4l-1,1L48.5,95.3a4.7,4.7,0,0,0-1.6,3.2,5.1,5.1,0,0,0,3,4.7,4.2,4.2,0,0,0,5.1-.9l10-10c.8-.8,1.3-1,2.2-.1,3.2,3.4,6.5,6.6,9.8,9.9a4.7,4.7,0,0,0,3,1.6,5.1,5.1,0,0,0,4.8-3,4.3,4.3,0,0,0-1-5.1l-13-13.1-1-1.1a38.9,38.9,0,0,0,9.4-2.1,23,23,0,0,0,8.2-5.3c1.5-1.6,1.5-3.5.3-5.5a4.3,4.3,0,0,0-5-2A7.7,7.7,0,0,0,80,67.9a25.6,25.6,0,0,1-28.3-.3l-1.3-.8a4.7,4.7,0,0,0-5.3,7.6l1.6,1.4c4.5,3.5,9.8,5,15.6,5.6Zm3.6-14.9A19,19,0,1,0,47,47.4,19,19,0,0,0,65.8,66.5Zm9.5-19.1A9.3,9.3,0,1,0,66,56.7,9.3,9.3,0,0,0,75.3,47.4Z"/></g></g></svg></a>
					
				<a class="share-links--a" href="https://connect.mail.ru/share?url=http%3A%2F%2Fnew-smile%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d0%25b8%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d1%258c-5-2%2F&amp;title=%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D1%8C%205%20%E2%80%94%20Smile%20English&amp;utm_source=share2" rel="nofollow noopener" target="_blank" title="Мой Мир"><svg xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 132 132"><defs><style>.cls-1{fill:#6c6c6c;}</style></defs><title>Мой Мир</title><g id="Слой_2" data-name="Слой 2"><g id="Слой_1-2" data-name="Слой 1"><path class="share-links--img" d="M122.4,0H9.6A9.6,9.6,0,0,0,0,9.6V122.4A9.6,9.6,0,0,0,9.6,132H122.4a9.6,9.6,0,0,0,9.6-9.6V9.6A9.6,9.6,0,0,0,122.4,0ZM90.5,83.1a13.9,13.9,0,0,1-11.3-5.9A17.4,17.4,0,1,1,76.6,52h0a3.5,3.5,0,0,1,6.9,0V69.2a6.9,6.9,0,1,0,13.9,0c0-23.2-15.4-34.7-31.2-34.7a31.2,31.2,0,0,0,0,62.5,30.9,30.9,0,0,0,19-6.4c3.7-2.8,7.9,2.7,4.2,5.5a37.8,37.8,0,0,1-23.2,7.9,38.2,38.2,0,0,1,0-76.4c19.2,0,38.2,14.1,38.2,41.6A13.9,13.9,0,0,1,90.5,83.1Z"/><circle class="share-links--img" cx="66" cy="66" r="8.6"/></g></g></svg></a>
					
				<a class="share-links--a" href="https://twitter.com/intent/tweet?text=%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D1%8C%205%20%E2%80%94%20Smile%20English&amp;url=http%3A%2F%2Fnew-smile%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d0%25b8%2F%25d0%25bd%25d0%25be%25d0%25b2%25d0%25be%25d1%2581%25d1%2582%25d1%258c-5-2%2F&amp;utm_source=share2" rel="nofollow noopener" target="_blank" title="Twitter"><svg xmlns="http://www.w3.org/2000/svg" width="40px" height="40px" viewBox="0 0 132 132"><title>Twitter</title><g id="Слой_2" data-name="Слой 2"><g id="_1" data-name="1"><path class="share-links--img" d="M9.6,0H122.4A9.6,9.6,0,0,1,132,9.6V122.4a9.6,9.6,0,0,1-9.6,9.6H9.6A9.6,9.6,0,0,1,0,122.4V9.6A9.6,9.6,0,0,1,9.6,0Zm91.9,44a29.2,29.2,0,0,1-8.4,2.3,14.7,14.7,0,0,0,6.4-8.1,29,29,0,0,1-9.3,3.5,14.6,14.6,0,0,0-25.2,10,15.2,15.2,0,0,0,.4,3.3,41.4,41.4,0,0,1-30-15.2,14.6,14.6,0,0,0,4.5,19.4,14.3,14.3,0,0,1-6.6-1.8v.2A14.6,14.6,0,0,0,45,71.9a15.1,15.1,0,0,1-3.8.5l-2.8-.3A14.6,14.6,0,0,0,52.1,82.3,29.3,29.3,0,0,1,34,88.5l-3.5-.2a41.2,41.2,0,0,0,22.3,6.5c26.8,0,41.4-22.2,41.4-41.4V51.5A29.4,29.4,0,0,0,101.5,44Z"/></g></g></svg></a>
					
			</div>

			<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
			<script src="//yastatic.net/share2/share.js"></script>
			<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,twitter"></div>

	</article>
</main>


<!-- 	<div id="primary" class="content-area">
		<main id="main" class="site-main"> -->
			<!-- <h1>Нововофыовфшощы</h1> -->

		<?php
		// while ( have_posts() ) :
			// the_post();

			// get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		// endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->


<script>
	var x=0;
	$('#next').click(function (){
		x++;
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','-300px');
			$('#block2').css('left','0px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','-300px');
			$('#block3').css('left','0px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','-300px');
			$('#block4').css('left','0px');
			// alert(x);
		    break;
		   default:
		  	// alert(x);
		   	x--;
		   break;
		}
	});
	$('#prev').click(function moveClient(){
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','0px');
			$('#block2').css('left','300px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','0px');
			$('#block3').css('left','300px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','0px');
			$('#block4').css('left','300px');
			// alert(x);
		    break;
		  default:
		  	// alert(x);
		   	x++;
		   break;
		}
		x--;
	});
	var y=0;
	$('#next-a').click(function (){
		y++;
		switch(y) {
		  case 1:   // if (x === 'value1')
		    $('#blocka1').css('left','-300px');
			$('#blocka2').css('left','0px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#blocka2').css('left','-300px');
			$('#blocka3').css('left','0px');
			// alert(x);
		    break;

		  case 3:
			$('#blocka3').css('left','-300px');
			$('#blocka4').css('left','0px');
			// alert(x);
		    break;
		   default:
		  	// alert(x);
		   	y--;
		   break;
		}
	});
	$('#prev-a').click(function moveClient(){
		switch(y) {
		  case 1:   // if (x === 'value1')
		    $('#blocka1').css('left','0px');
			$('#blocka2').css('left','300px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#blocka2').css('left','0px');
			$('#blocka3').css('left','300px');
			// alert(x);
		    break;

		  case 3:
			$('#blocka3').css('left','0px');
			$('#blocka4').css('left','300px');
			// alert(x);
		    break;
		  default:
		  	// alert(x);
		   	y++;
		   break;
		}
		y--;
	});
</script>




<?php
// get_sidebar();
get_footer();
