<?php
/*


 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

<main class="content">
	<div class="price-con">
			<h3 class="price--h3">Групповые занятия</h3>
			<table class="price__table">
				<thead class="table__head">
					<tr class="table__head--tr">
						<td class="table__head--td">Направление</td>
						<td class="table__head--td">Режим</td>
						<td class="table__head--td">Длительность занятия</td>
						<td class="table__head--td">Стоимость абоненмента <br> (8 занятий)</td>
					</tr>
					<tr class="table__head--color"></tr>
				</thead>
				<tbody>
					<?php
					$key_mode ="режим";
					$key_time ="длительность";
					$key_price ="цена";
					$myposts = get_posts( array(
						'category' => 11
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

					?>
					<tr class="table--tr">
						<td class="table--td"><?php the_title();?></td>
						<td class="table--td">
							<?php the_field($key_mode);?>
						</td>
						<td class="table--td">
							<?php the_field($key_time);?>
						</td>
						<td class="table--td">
							<?php the_field($key_price); ?> ₽
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>

			<h3 class="price--h3">Подготовка к экзаменам (ЕГЭ / ОГЭ / Euroexam / FCE / IELTS)</h3>
			<table class="price__table">
				<thead class="table__head">
					<tr class="table__head--tr">
						<td class="table__head--td">Направление</td>
						<td class="table__head--td">Режим</td>
						<td class="table__head--td">Длительность занятия</td>
						<td class="table__head--td">Стоимость абоненмента <br> (8 занятий)</td>
					</tr>
				</thead>
				<tbody>
					<?php
					$myposts = get_posts( array(
						'category' => 14
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

					?>
					<tr class="table--tr">
						<td class="table--td"><?php the_title();?></td>
						<td class="table--td">
							<?php the_field($key_mode);?>
						</td>
						<td class="table--td">
							<?php the_field($key_time);?>
						</td>
						<td class="table--td">
							<?php the_field($key_price);?> ₽
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>

			<h3 class="price--h3">Индивидуальные занятия</h3>
			<table class="price__table">
				<thead class="table__head">
					<tr class="table__head--tr">
						<td class="table__head--td">Направление</td>
						<td class="table__head--td">Режим</td>
						<td class="table__head--td">Длительность занятия</td>
						<td class="table__head--td">Стоимость абоненмента <br> (8 занятий)</td>
					</tr>
				</thead>
				<tbody>
					<?php
					$myposts = get_posts( array(
						'category' => 15
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

					?>
					<tr class="table--tr">
						<td class="table--td"><?php the_title();?></td>
						<td class="table--td">
							<?php the_field($key_mode);?>
						</td>
						<td class="table--td">
							<?php the_field($key_time);?>
						</td>
						<td class="table--td">
							<?php the_field($key_price);?> ₽
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
	</div>
</main>


<!-- 	<div id="primary" class="content-area">
		<main id="main" class="site-main"> -->
			<!-- <h1>Нововофыовфшощы</h1> -->

		<?php
		// while ( have_posts() ) :
			// the_post();

			// get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) :
			// 	comments_template();
			// endif;

		// endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->


<script>
	var x=0;
	$('#next').click(function (){
		x++;
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','-300px');
			$('#block2').css('left','0px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','-300px');
			$('#block3').css('left','0px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','-300px');
			$('#block4').css('left','0px');
			// alert(x);
		    break;
		   default:
		  	// alert(x);
		   	x--;
		   break;
		}
	});
	$('#prev').click(function moveClient(){
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','0px');
			$('#block2').css('left','300px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','0px');
			$('#block3').css('left','300px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','0px');
			$('#block4').css('left','300px');
			// alert(x);
		    break;
		  default:
		  	// alert(x);
		   	x++;
		   break;
		}
		x--;
	});
</script>

<?php
// get_sidebar();
get_footer();
