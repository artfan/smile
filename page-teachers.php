<?php
/**
Шаблон страницы Преподаватели подключен через slug

 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header>
				<h1 class="header--h1">
					<?php the_title();?> 
				</h1>
			</header>
		<section class="prepods__con">
			<?php
				$i = 1;
					$myposts = get_posts( array(
						'category' => 17,
						'numberposts' => 100,
					) );

					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;

			?>

			<article class="prepod-one" id="prepod<?php echo ($i); ?>">
				<div class="prepod-one__photos">
					<?php
						$image1 = get_field('фото1');
						$image2 = get_field('фото2');
					?>
					<div class="photo-con">
						<img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" class="prepods-one__photo prepods-one__photo--second"/>
						<img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" class="prepods-one__photo prepods-one__photo--first"/>
					</div>
				</div>
				<div class="prepod-one__desc">
					<h2 class="prepod-one__desc--h2">
						<?php the_title();?>
					</h2>
					<div class="desc__info">
						<?php
							$value = get_field( "образование" ); 
							if ($value): ?>
							<h4 class="desc__info--h4">
								<i class="fas fa-user-graduate" ></i>
								Образование
							</h4>

							<p>
								<?php the_field('образование');?>
							</p>
								
							<h4 class="desc__info--h4">
								<i class="fas fa-business-time" ></i>
								Опыт работы
							</h4>
							<p>
								<?php the_field('опыт_работы');?>
							</p>
						<?php endif ?>
						<div class="info--content">
							<?php the_content();?>
						</div>
			
					</div>
				</div>
				<?php $i++; ?> 
			</article>
			<?php } ?>
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
