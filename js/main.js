// СЛАЙДЕР ПРЕПОДАВАТЕЛЕЙ НА ГЛАВНОЙ
$('.slider-con').slick({
  infinite: true,
  autoplay: true,
	// autoplaySpeed: 1000,
  slidesToShow: 4,
  slidesToScroll: 1,
  swipe: true,
  prevArrow: '#prev-prepod',
	nextArrow: '#next-prepod',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

// СЛАЙДЕР НОВОСТЕЙ И АКЦИИ

$('.aside__news--slider').slick({
  infinite: true,
  autoplay: true,
  // autoplaySpeed: 1000,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipe: true,
  prevArrow: '#prev',
  nextArrow: '#next'
});
$('.aside__sale--slider').slick({
  infinite: true,
  autoplay: true,
  // autoplaySpeed: 1000,
  slidesToShow: 1,
  slidesToScroll: 1,
  swipe: true,
  prevArrow: '#prev-a',
  nextArrow: '#next-a'
});

// ПОПАП ОКНА

$(function() {
    $('.menu__icon').on('click', function() {
      $(this).closest('.menu').toggleClass('menu_state_open');
    });
  });

  function popup() {
    document.getElementById('popup-con').style.display = "block";
    $(".popup__map-close").click(function(){  
    // Событие клика на затемненный фон
      document.getElementById('popup-con').style.display = "none";
    });
    $(".popup_bg__map").click(function(){  
    // Событие клика на затемненный фон
      document.getElementById('popup-con').style.display = "none";
    });
  }

