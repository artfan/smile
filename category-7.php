<?php
/*

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>

<section class="content">
	<?php
		get_sidebar();
	?>
	<aside class="aside__news">
		<div class="aside__news-con">
			<div class="aside__news--arrow">
				<button id="prev" class="aside__news--btn" href="#"><i class="fas fa-angle-left"></i></button>
			</div>
			<div style="position: relative; overflow: hidden; flex-basis: 80%; width: 220px;height: 300px;">
			<?php
				$i = 1;
				function position($a){
					if ($a==1){
						echo "left: 0;";
					} else {
						echo "left: 300px";
					}
				}

				$myposts = get_posts( array(
					'category' => 6
				) );

				foreach( $myposts as $post ){
				setup_postdata( $post );
				$a = True;
					if ($i == 5) {
						$a = False;
					}
				?>	
				<div class="aside__news__n-one" id="block<?php echo($i);?>" style="<?php position($i);?>">
					<a href="<?php the_permalink() ?>" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></a>
					<?php
						$default_attr = array('class' => "n-one--img");
						the_post_thumbnail(array(420,280),$default_attr);
					?>
					<h5 class="n-one--h5"><?php the_title(); ?></h5>
					<p class="n-one--p">
						<?php
							the_excerpt();
						?>
					</p>
				</div>
				<?php
					$i++;
				}
					wp_reset_postdata();
				?>
			</div>

			<div class="aside__news--arrow">
				<button class="aside__news--btn" id="next" href="#"><i class="fas fa-angle-right"></i></button>
			</div>
		</div>
	</aside> 

	<article class="one-article" style="border: none; padding: 0;">
		<div class="feedbaks__feed-all">

			<?php if ( have_posts() ) : 
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				?>
				<article class="feed-all__one-feed">
					<?php if(has_post_thumbnail()){
						?>
						<div class="one-feed__img">
							<?php
							$default_attr = array('class' => "one-feed__img--img");
							the_post_thumbnail(array(420,280),$default_attr);
							?>
						</div>
					<div class="one-feed__text">
						<p class="one-feed__text--name-p"><?php the_title(); ?></p>
						<!-- <p class="one-feed__text--text-p"> -->
							<?php the_content(); ?>
						<!-- </p> -->
					</div>
					<?php } else{?>
						<div class="one-feed__text" style="width: 100%;">
						<p class="one-feed__text--name-p"><?php the_title(); ?></p>
						<!-- <p class="one-feed__text--text-p"> -->
							<?php the_content(); ?>
						<!-- </p> -->
					</div>

					<?php } ?>
						
				</article>
	
			<?php
			endwhile;
			endif;
			?>
				<!-- <div class="one-feed__img">
					<img src="img/feedbacks/1.png" alt="" class="one-feed__img--img">
				</div> -->

		</div>
	</article>
</section>
<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script>
	var x=0;
	$('#next').click(function (){
		x++;
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','-300px');
			$('#block2').css('left','0px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','-300px');
			$('#block3').css('left','0px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','-300px');
			$('#block4').css('left','0px');
			// alert(x);
		    break;
		   default:
		  	// alert(x);
		   	x--;
		   break;
		}
	});
	$('#prev').click(function moveClient(){
		switch(x) {
		  case 1:   // if (x === 'value1')
		    $('#block1').css('left','0px');
			$('#block2').css('left','300px');
			// alert(x);
		    break;

		  case 2:  // if (x === 'value2')
			$('#block2').css('left','0px');
			$('#block3').css('left','300px');
			// alert(x);
		    break;

		  case 3:
			$('#block3').css('left','0px');
			$('#block4').css('left','300px');
			// alert(x);
		    break;
		  default:
		  	// alert(x);
		   	x++;
		   break;
		}
		x--;
	});
</script>
<?php

get_footer();
