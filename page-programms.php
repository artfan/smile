<?php
/**
Шаблон страницы Программы подключен через slug

 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smile_English
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header>
				<h1 class="header--h1">
					<?php the_title();?> 
				</h1>
			</header>
		<h2 class="program--h2">Английский язык для взрослых</h2>
		<section class="program__con">
			<?php
				$myposts = get_posts( array(
					'category' => 8
				) );
				foreach( $myposts as $post ){
					setup_postdata( $post );
					$id = $post->ID;
			?>
			<div class="program__one-program">
				<img src="<?php echo (get_the_post_thumbnail_url());?>" alt="">
				<a class="program__one-program--h3" href="<?php the_permalink(); ?>">
					<h3 class="program__one-program--h3">
						<?php the_title();?>
					</h3>
				</a>
				
				<?php
					// the_excerpt();
				?>
				<a class="program__one--more" href="<?php the_permalink(); ?>">Подробнее</a>
					
			</div>
		<?php
			}
		?>
		</section>
		
		<h2 class="program--h2">Английский язык для детей</h2>
		
		<section class="program__con">

			<?php
					$myposts = get_posts( array(
						'category' => 9
					) );
					foreach( $myposts as $post ){
						setup_postdata( $post );
						$id = $post->ID;
			?>
			<div class="program__one-program">
				<img class="program__header--img" src="<?php echo (get_the_post_thumbnail_url());?>" alt="">
				<a class="program__one-program--h3"href="<?php the_permalink(); ?>">
					<h3 class="program__one-program--h3">
						<?php the_title();?>
					</h3>
				</a>
				
				<?php
					// the_excerpt();
				?>
				<a class="program__one--more" href="<?php the_permalink(); ?>">Подробнее</a>
			</div>

			<?php } ?>
			
		</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
// get_sidebar();
get_footer();
