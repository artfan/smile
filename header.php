<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Smile_English
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


<header id="masthead" class="header">
	<div class="header-cont">
		<div class="header__logo">
			<?php
				the_custom_logo();
			?>
		</div>
		<div class="header__address">
			<i class="fas fa-map-marker-alt header__address--marker"></i>
			<div class="header__address-con">
				<a class="header__address--a" href="#map">Абакан, ул. Кати Перекрещенко 7 </a>
				<a class="header__address--a" href="#map">Абакан, ул. Торосова 9</a>
			</div>
		</div>
		<div class="header__phone">
			<a href="tel:89024670055" class="header__phone--a">
				<i class="fas fa-phone"></i>
				8 (902) 467-00-55
			</a>
			<a href="tel:89232134466" class="header__phone--a">
				<i class="fas fa-phone"></i>
				8 (923) 213-44-66
			</a>
		</div>
		
		<div class="header__mails">
			
			<button class="header__mails--callback" onclick="popup()">Связаться с нами</button>
		</div>
		<div class="header__soc-links">
			<a href="https://vk.com/smileeng" class="soc-links--a soc-links--vk" target="_blank"><i class="fab1 fab fa-vk"></i></a>
			<a href="https://www.facebook.com/groups/1744453485796586" class="soc-links--a soc-links--fb" target="_blank"><i class="fab1 fab fa-facebook-f"></i></a>
			<a href="https://www.instagram.com/smileenglishabakan/" class="soc-links--a soc-links--inst" target="_blank"><i class="fab1 fab fa-instagram"></i></a>
		</div>
	</div>
</header>

	<div class="m-header">
		<div class="header__logo">
			<?php
				the_custom_logo();
			?>
			
		</div>
		<div class="header__phone">
			<a href="tel:89024670055" class="header__phone--a">
				<i class="fas fa-phone"></i>
				8 (902) 467-00-55
			</a>
			<a href="tel:83902260055" class="header__phone--a">
				<i class="fas fa-phone"></i>
				8 (3902) 26-00-55
			</a>
		</div>

		<div class="header__address">
			<i style="font-size: 40px; margin-right: 10px;" class="fas fa-map-marker-alt"></i>
			<div class="header__address-con">
				<a class="header__address--a" href="#map">ул. Кати Перекрещенко 7 </a>
				<a class="header__address--a" href="#map">ул. Торосова 9</a>
			</div>
		</div>	
		
	</div>
<div class="menu">
	<div class="m-menu">	
		<div class="header__mails">
			<button class="header__mails--callback" onclick="popup()">Связаться с нами</button>
		</div>

	    <div class="menu__icon">
	      <span></span>
	      <span></span>
	      <span></span>
	      <span></span>
	    </div>
	</div>

	<?php
	wp_nav_menu( array(
		'theme_location' => 'menu-1',
		'menu_id'        => 'primary-menu',
		'container' => 'nav',
		'container_class' => 'main-nav',
		'menu_class' => 'main-nav--ul',
	) );
	?>

</div>
		<!-- #site-navigation -->
<script>

  
</script>

<div class="popup__map" id="popup-con">
	<div class="popup_bg__map">
	</div>
		<div class="popup__map-con">
			<div class="contacts__form">
				<h3 class="info__form--h3">В ближайшее время мы ответим на ваши вопросы</h3>
				<?php
					echo do_shortcode('[contact-form-7 id="53" title="ФОС контакты"]');
				?>
			</div>
		</div>
	<div class="popup__map-close" id="map__close"> 
		<i class="popup__close--i fas fa-times"></i>
	</div>
</div>

<!-- <div id="page" class="site"> -->
	<!-- <div id="content" class="site-content"> -->
